Gitlab CI runner role
=========

A role to run Gitlab CI. When run it sets up a CI runner with the docker executor as default. 
Can optionally configure a second runner with docker_dind.

Requirements
------------

Docker installed is required. Run with [docker role](../docker) and [centos7-system](../centos7-system)
`gitlab_runner_executor.desc` must be unique. Current check if a runner should be created is a simple lookup on if there exists a runner in the config.toml file with desc name. Removing roles should be done by rebuilding the instance, since the role doesn't handle deletions.

Role Variables
--------------
See example playbook.

Dependencies
------------

Example Playbook
----------------
```

- name: "Set up gitlab runner"
  remote_user: centos
  hosts: "gitlab_runner"
  tasks:
  - import_role:
      name:  centos7-system
    vars:
      epel_repo: true
  - import_role:
      name: docker
  - import_role:
      name: "gitlab-runner"
    vars:
      gitlab_runner_executor:
      - token: "{{ gitlab_token }}"
        gitlab_runner_url: "https://gitlab.com" #default https://git.app.uib.no
        desc: "Example runner gitlab"
        tags: "example"
      - token: "{{ gitapp_token }}"
        desc: "Example runner uib"
        tags: "example"

```

TODO
-------
* Add some global config settings (concurrent runners) to update config.toml.
* Add support for non docker executors by adding executor as parameter to gitlab_runner_executor (with docker as default)
* Add a check that fails if same desc is used multiple times.

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
